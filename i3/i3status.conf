# i3status configuration file.
# see "man i3status" for documentation.

# It is important that this file is edited as UTF-8.
# The following line should contain a sharp s:
# ß
# If the above line is not correctly displayed, fix your editor first!

general {
        colors = true
        interval = 5
        markup = "pango"
}

order += "razer_mouse"
order += "lm_sensors"
order += "nvidia_smi"
order += "memory"
order += "uname"
order += "keyboard_locks"
order += "tztime local"

ethernet _first_ {
        format_up = "E: %speed"
        format_down = "E: down"
}

uname {
        format = "{release}"
}

memory {
        format = "%used<span color='darkgray'>|</span>%available"
        threshold_degraded = "1G"
        format_degraded = "MEMORY < %available"
}

tztime local {
        format = "%Y-%m-%d <span color='lightgreen'>%H:%M</span>"
}

lm_sensors {
    chips = ['k10temp-pci-00c3']
    sensors = ['tctl']
    format_chip = '[\?if=name=k10temp-pci-00c3 CPU ]{format_sensor}'
    format_sensor = '[\?color=input&show {input:02.2f}°C]'
    thresholds = [(0,'good'),(82,'bad')]
}

nvidia_smi {
    format_gpu = 'GPU [\?color=temperature.gpu {temperature.gpu}°C] '
    format_gpu += '[\?color=memory.used_percent {memory.used} {memory.used_unit}'
    format_gpu += '[\?color=darkgray&show \|]{memory.used_percent:.1f}%]'
}

razer_mouse {
    dev_number = 0
    format = "{percent}% {batt_icon}"
}

keyboard_locks {
    format = "[\?if=num_lock |\?color=bad NUM][\?soft  ]"
    format += "[\?if=caps_lock&color=good CAPS][\?soft  ]"
    format += "[\?if=scroll_lock&color=good SCR]"
}
